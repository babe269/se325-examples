package se325.lecture02.advancedgreetings.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GreetingService extends Remote {

    Greeting getGreeting(String name) throws RemoteException;

}