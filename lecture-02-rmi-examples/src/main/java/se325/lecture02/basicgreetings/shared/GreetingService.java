package se325.lecture02.basicgreetings.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GreetingService extends Remote {

    String getGreeting(String name) throws RemoteException;

}