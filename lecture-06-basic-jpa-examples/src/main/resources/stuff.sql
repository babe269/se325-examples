select u.USERNAME, u.ADDRESS, bd.ACCOUNT, bd.BANKNAME, o.DESCRIPTION
    from USERS u
        left outer join BILLINGDETAILS bd on bd.USER_ID = u.ID
        left outer join ORDERS o on o.USER_ID = u.ID
    where u.ID = 1;