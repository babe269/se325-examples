package se325.lecture05.parolee.domain;

/**
 * Simple enumeration for representing Gender.
 *
 */
public enum Gender {
	MALE, FEMALE, OTHER;
}
