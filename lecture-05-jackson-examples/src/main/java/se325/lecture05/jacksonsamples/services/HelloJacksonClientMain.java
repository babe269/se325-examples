package se325.lecture05.jacksonsamples.services;

import se325.lecture05.jacksonsamples.example01_basic.Book;
import se325.lecture05.jacksonsamples.example01_basic.Genre;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class HelloJacksonClientMain {

    public static void main(String[] args) {

        Client client = ClientBuilder.newClient();

        Book book = new Book("The Neverending Story", Genre.Fantasy);
        System.out.println("Original book: " + book);

        Response response = client.target("http://localhost:10000/lecture_05_examples_war_exploded/helloJacksonServices/hello/book")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(book));


        Book responseBook = response.readEntity(Book.class);
        System.out.println("Echoed book: " + responseBook);

        client.close();

    }

}
